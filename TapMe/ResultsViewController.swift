//
//  ResultsViewController.swift
//  TapMe
//
//  Created by Thomas Black on 21/09/2014.
//  Copyright (c) 2014 Thomas Black. All rights reserved.
//

import UIKit

class ResultsViewController: UIViewController {
    var shareTextHolder = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        var countHolder = count
        
        
        if highscore > countHolder {
                countResults.text = "Congratulations, You scored \(countHolder)"
            shareTextHolder = "Congratulations, You scored \(countHolder)"
            
        } else if highscore < countHolder {
                countResults.text = "Awesome Job! You beat your record at \(countHolder)"
                highscore = countHolder
                userDefaults.setInteger(highscore, forKey: "highscore")
                userDefaults.synchronize()
            shareTextHolder = "Awesome Job! You beat your record at \(countHolder)"
        }
        println(highscore)
    

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBOutlet var countResults: UILabel!
    
    @IBAction func share() {
        shareTextImageAndURL(shareTextHolder, sharingImage: nil, sharingURL: nil)
        
    }
    
    @IBAction func playAgain() {
        timesPlayed++
        
        println(timesPlayed)
        println(instanceTimesPlayed)
        
        userDefaults.setInteger(timesPlayed, forKey: "timesPlayed")
        userDefaults.synchronize()
    }
    
    
    func shareTextImageAndURL(sharingText: String?, sharingImage: UIImage?, sharingURL: NSURL?) {
        var sharingItems = [AnyObject]()
        
        if let text = sharingText {
            sharingItems.append(sharingText!)
        }
        if let image = sharingImage {
            sharingItems.append(sharingImage!)
        }
        if let url = sharingURL {
            sharingItems.append(sharingURL!)
        }
        
        var services = [AnyObject]()
        //services = [,]
        
        let activityViewController = UIActivityViewController(activityItems: sharingItems, applicationActivities: nil)
        
        self.presentViewController(activityViewController, animated: true, completion: nil)
    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
