//
//  ViewController.swift
//  TapMe
//
//  Created by Thomas Black on 21/09/2014.
//  Copyright (c) 2014 Thomas Black. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    //for sharing
    var screenshot: UIImage?
    
    @IBOutlet var scoreLabel:UILabel!
    @IBOutlet var timeLabel:UILabel!
    
    @IBAction func buttonPressed() {
        count++
        
        if count == 1 {
            timer = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: "subtractTime", userInfo: nil, repeats: true)
        }
        
        scoreLabel.text = "Score: \(count)"
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        setupGame()
        userDefaults.synchronize() //!! all
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func setupGame() {
        seconds = 9
        count = 0
        
        scoreLabel.text = "Score: \(count)"
        timeLabel.text = "Time: 10"
    
    }
    
    func subtractTime() {
        timeLabel.text = "Time: \(seconds)"
        seconds--
        
        
        if seconds == 0 {
            timer.invalidate()
            //!! highscore
            
            performSegueWithIdentifier("moveToResults", sender: self)
            screenShotMethod()
            setupGame()
            
        }
    }
    func screenShotMethod() {
        //Create the UIImage
        UIGraphicsBeginImageContext(view.frame.size)
        view.layer.renderInContext(UIGraphicsGetCurrentContext())
        let screenShotImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        screenshot = screenShotImage
        
        //Save it to the camera roll
        //UIImageWriteToSavedPhotosAlbum(screenShotImage, nil, nil, nil)
    }
    
}

