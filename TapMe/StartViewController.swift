//
//  StartViewController.swift
//  TapMe
//
//  Created by Thomas Black on 22/09/2014.
//  Copyright (c) 2014 Thomas Black. All rights reserved.
//

import UIKit

class StartViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var highscoreLabelHolder = userDefaults.integerForKey("highscore")
        highscoreLabel.text = "Highscore: \(highscoreLabelHolder)"
        
        var timesPlayedLabelHolder = userDefaults.integerForKey("timesPlayed")
        timesPlayedLabel.text = "Times Played: \(timesPlayedLabelHolder)"
        
        //printoutTimesPlayed = userDefaults.integerForKey("timesCount")
        //timesPlayedLabel.text = "Times Played: \(printoutTimesPlayed)"
        //timesPlayedLabel.text = userDefaults.valueForKey(timesplayedstored)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBOutlet var highscoreLabel:UILabel!
    
    @IBOutlet var timesPlayedLabel:UILabel!
    
    
    @IBAction func play() {
        timesPlayed++
        userDefaults.setInteger(timesPlayed, forKey: "timesPlayed")
        userDefaults.synchronize()

    }
    
    
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
